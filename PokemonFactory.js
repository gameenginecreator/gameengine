	//Exemple de création de level, mettre tous les levels ici
var TypePok =
{
	Proglax:1,
	Kary:2,
	Digcky:3,
	Wobburush:4,
	Vichu:5
};

function PokemonFactory()
{
}

PokemonFactory.prototype.generatePokemon = function( Type, Side )
{
	var Pok;
	var TabAtt = new Array();	
	switch (Type)
	{
		case TypePok.Proglax: var ProgSprite = new Sprite( Resource.get("img/Pokemon/proglax.png"),0,0,0,0, 27 ,27,0,0,8 );
							
							TabAtt.push( new Attaque("Pound",35,40,100,"Physique") );
							TabAtt.push( new Attaque("Growl",40,0,100,"Autre") );
							Pok =  new Pokemon(Type,1,60,30,65,160,TabAtt,ProgSprite, Side) ;
		break;
		case TypePok.Kary: 	var KarySprite = new Sprite( Resource.get("img/Pokemon/karianne.png"),0,0,0,0, 27 ,27,0,0,8 );
							TabAtt.push( new Attaque("Pound",35,40,100,"Physique") );
							TabAtt.push( new Attaque("Growl",40,0,100,"Autre") );
							Pok = new Pokemon(Type,1,25,5,50,250, TabAtt,KarySprite, Side);
		break;
		case TypePok.Digcky: 	var DigckySprite = new Sprite( Resource.get("img/Pokemon/Vickie.png"),0,0,0,0, 27 ,27,0,0,8 );
							TabAtt.push( new Attaque("Scratch",30,40,100,"Physique") );
							TabAtt.push( new Attaque("Growl",40,0,100,"Autre") );
							Pok = new Pokemon(Type,1,55,95,25,250, TabAtt,DigckySprite, Side);
		break;
		case TypePok.Wobburush: 	var WobburushSprite = new Sprite( Resource.get("img/Pokemon/Wobburush.png"),0,0,0,0, 27 ,27,0,0,8 );
							TabAtt.push( new Attaque("Pound",45,40,100,"Physique") );
							TabAtt.push( new Attaque("Growl",40,0,100,"Autre") );
							Pok = new Pokemon(Type,1,33,33,58,190, TabAtt,WobburushSprite, Side);
		break;
		case TypePok.Vichu: 	var VichuSprite = new Sprite( Resource.get("img/Pokemon/Vichu.png"),0,0,0,0, 27 ,27,0,0,8 );
							TabAtt.push( new Attaque("thundershock",40,40,100,"Physique") );
							TabAtt.push( new Attaque("Growl",40,0,100,"Autre") );
							Pok = new Pokemon(Type,1,75,90,35,250, TabAtt,VichuSprite, Side);
		break;					
	}
	return Pok;
}

//Pokemon(Type,Niveau,Attaque,Vitesse,Def,Vie,TabAttaque,Sprite,SidePok)
//Attaque(Nom,PP,Puissance,Precision,Classe)
