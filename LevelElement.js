function LevelElement(Rect,SpriteSheet,SpriteX,SpriteY,Scale )
{
	this.base = Entity;
	this.base();
	
	this.Box = Rect;
	this.Scale = Scale;
	this.Sprite  = new Sprite( SpriteSheet,SpriteX,SpriteY,this.Box.Pos.X,this.Box.Pos.Y,this.Box.Width ,this.Box.Height,0,0,this.Scale );
}

LevelElement.prototype = new Entity;

LevelElement.prototype.draw = function(Context)
{
	this.Sprite.draw(Context);
	if( GlobalValue.DebugMode )
	{
		drawDebugRect(this,Context);
	}
}
var BoxType = {
	BoxType_Collision : 0,
	BoxType_Teleport : 1,
	BoxType_Dialog : 2,
	BoxType_Combat : 3
};


function LevelBox(Rect)
{

	this.base = Entity;
	this.base();
	
	this.Box = Rect;
	this.Scale = 1;
	this.Type = BoxType.BoxType_Collision;
	this.NextLevel = LevelName.Level_None;
}

LevelBox.prototype = new Entity;

LevelBox.prototype.draw = function(Context)
{
	if( GlobalValue.DebugMode )
	{
		drawDebugRect(this,Context);
	}
}

console.log('LevelElement.js loaded');