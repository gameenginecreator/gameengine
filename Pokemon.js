var SidePokemon = {
	SideLeft : 0,
	SideRight: 1
};

function Pokemon(Type,Niveau,Attaque,Vitesse,Def,Vie,TabAttaque,Sprite,SidePok)
{
	this.Type = Type;
	this.Name = getName( Type );
	this.Niveau = Niveau;
	this.Attaque = Attaque;
	this.Vitesse = Vitesse;
	this.Def = Def;
	this.Vie = Vie;
	this.TabAttaque = TabAttaque;
	this.Sprite = Sprite;
	this.SidePok = SidePok;

	this.setSide( this.SidePok );
}

function getCurrentLife(Pok){
	return Pok.Vie;
}

function Attaque(Nom,PP,Puissance,Precision,Classe)
{
	this.Nom = Nom;
	this.PP= PP;
	this.Puissance = Puissance;
	this.Precision = Precision;
	this.Classe = Classe;
}

Pokemon.prototype = new Entity;

Pokemon.prototype.setSide = function ( Side )
{
	this.SidePok = Side;
	if( this.SidePok == SidePokemon.SideLeft  )
	{
		this.Sprite.X = 170;
		this.Sprite.Y = 260;
		this.Sprite.Scale = 4;
	}
	else
	{
		this.Sprite.X = 518;
		this.Sprite.Y = 61;
		this.Sprite.Scale =4;
	}
}


function getName( Type )
{
	switch (Type)
	{
		case TypePok.Proglax: return "Proglax";
		break;
		case TypePok.Kary: return "Kary";
		break;
		case TypePok.Vichu: return "Vichu";
		break;
		case TypePok.Digcky: return "Digcky";
		break;
		case TypePok.Wobburush: return "Wobburush";
		break;
	}
}

Pokemon.prototype.draw = function(Context)
{
	this.Sprite.draw(Context);
	
	if( GlobalValue.DebugMode )
	{
		drawDebugRect(this,Context);
	}
}