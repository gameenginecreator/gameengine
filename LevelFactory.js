
//Exemple de cr�ation de level, mettre tous les levels ici
var LevelName =
{
	Level_None: 0,
	Level_1 : 1,
	Level_2 : 2,
	Level_3 : 3,
	Level_Fight:4,
	Level_Count : 5
};

function LevelFactory()
{
	this.Player = new Player( 0,0 );
}
LevelFactory.prototype.init = function ()
{
}


LevelFactory.prototype.generateLevel = function( LvlName )
{
	var BackGroundImage = new Array();
	var LevelEl = new Array();
	if( LvlName == LevelName.Level_1 )
	{
			BackGroundImage.push( new Sprite( Resource.get( "img/Map/Littleroot.png" ),0,0,0,0, GlobalValue.CanvasWidth, GlobalValue.CanvasHeight,0,0,1 ) );
			//level Collision
			LevelEl.push( new LevelBox( new Rect( new Pos(179,485),382,0)));
			LevelEl.push( new LevelBox( new Rect( new Pos(138,445),50,43)));
			LevelEl.push( new LevelBox( new Rect( new Pos(565,442),50,43)));
			LevelEl.push( new LevelBox( new Rect( new Pos(133,78),50,43)));
			LevelEl.push( new LevelBox( new Rect( new Pos(620,79),50,43)));
			LevelEl.push( new LevelBox( new Rect( new Pos(618,401),50,43)));
			LevelEl.push( new LevelBox( new Rect( new Pos(669,123),0,284)));
			LevelEl.push( new LevelBox( new Rect( new Pos(132,131),0,300)));
			LevelEl.push( new LevelBox( new Rect( new Pos(461,69),156,0)));
			LevelEl.push( new LevelBox( new Rect( new Pos(183,77),196,0)));
			LevelEl.push( new LevelBox( new Rect( new Pos(462,0),0,70)));
			LevelEl.push( new LevelBox( new Rect( new Pos(393,0),0,70)));
			LevelEl.push( new LevelBox( new Rect( new Pos(393,0),80,0)));
			//Maison collision
			LevelEl.push( new LevelBox( new Rect( new Pos(201,129),102,62)));
			LevelEl.push( new LevelBox( new Rect( new Pos(495,130),102,62)));
			LevelEl.push( new LevelBox( new Rect( new Pos(188,152),132,52)));
			LevelEl.push( new LevelBox( new Rect( new Pos(482,154),132,52)));
			LevelEl.push( new LevelBox( new Rect( new Pos(191,204),128,11)));
			LevelEl.push( new LevelBox( new Rect( new Pos(484,206),128,07)));
			LevelEl.push( new LevelBox( new Rect( new Pos(214,313),187,74)));
			LevelEl.push( new LevelBox( new Rect( new Pos(218,387),178,12)));
			//Pancarte Collision
			LevelEl.push( new LevelBox( new Rect( new Pos(294,427),26,3)));
			LevelEl.push( new LevelBox( new Rect( new Pos(533,335),26,3)));
			LevelEl.push( new LevelBox( new Rect( new Pos(454,221),26,3)));
			LevelEl.push( new LevelBox( new Rect( new Pos(320,221),26,3)));
			//Teleport Box
			var SpawnBox = new LevelBox( new Rect( new Pos(321,399),26,3)) ;
			SpawnBox.Type = BoxType.BoxType_Teleport;
			SpawnBox.NextLevel = LevelName.Level_2;
			LevelEl.push(SpawnBox);
			//little root to wood
			var LittleRootToWood = new LevelBox( new Rect( new Pos(392,0),68,20)) ;
			LittleRootToWood.Type = BoxType.BoxType_Teleport;
			LittleRootToWood.NextLevel = LevelName.Level_3;
			LevelEl.push(LittleRootToWood);
			//Combat Box
			var CombatBox = new LevelBox( new Rect( new Pos(453,437),26,3)) ;
			CombatBox.Type = BoxType.BoxType_Combat;
			CombatBox.NextLevel = LevelName.Level_Fight;

			LevelEl.push( CombatBox );
			LevelEl.push(  this.Player  );
																	
		return new Level( BackGroundImage,LevelEl ) ;
	}
	if( LvlName == LevelName.Level_2 )
	{
		BackGroundImage.push( new Sprite( Resource.get( "img/Map/Background.png" ),0,0,0,0, GlobalValue.CanvasWidth, GlobalValue.CanvasHeight,0,0,1 ) );
		LevelEl.push( new LevelBox( new Rect( new Pos(240,211),50,130)));
		LevelEl.push( new LevelBox( new Rect( new Pos(238,307),261,46)));
		LevelEl.push( new LevelBox( new Rect( new Pos(499,208),199,0)));
		LevelEl.push( new LevelBox( new Rect( new Pos(102,208),139,0)));
		LevelEl.push( new LevelBox( new Rect( new Pos(541,131),120,93)));
		LevelEl.push( new LevelBox( new Rect( new Pos(570,230),58,100)));
		LevelEl.push( new LevelBox( new Rect( new Pos(0,349),0,250)));
		LevelEl.push( new LevelBox( new Rect( new Pos(0,599),0,799)));
		LevelEl.push( new LevelBox( new Rect( new Pos(799,349),799,0)));
		LevelEl.push( new LevelBox( new Rect( new Pos(700,210),11,8)));
		LevelEl.push( new LevelBox( new Rect( new Pos(711,220),11,8)));
		LevelEl.push( new LevelBox( new Rect( new Pos(721,230),11,8)));
		LevelEl.push( new LevelBox( new Rect( new Pos(731,240),11,8)));
		LevelEl.push( new LevelBox( new Rect( new Pos(740,250),11,8)));
		LevelEl.push( new LevelBox( new Rect( new Pos(750,260),11,8)));
		LevelEl.push( new LevelBox( new Rect( new Pos(760,270),11,8)));
		LevelEl.push( new LevelBox( new Rect( new Pos(770,280),11,8)));
		LevelEl.push( new LevelBox( new Rect( new Pos(780,290),11,8)));
		LevelEl.push( new LevelBox( new Rect( new Pos(790,300),11,8)));
		LevelEl.push( new LevelBox( new Rect( new Pos(90,210),11,8)));
		LevelEl.push( new LevelBox( new Rect( new Pos(80,220),11,8)));
		LevelEl.push( new LevelBox( new Rect( new Pos(70,230),11,8)));
		LevelEl.push( new LevelBox( new Rect( new Pos(60,240),11,8)));
		LevelEl.push( new LevelBox( new Rect( new Pos(50,250),11,8)));
		LevelEl.push( new LevelBox( new Rect( new Pos(40,260),11,8)));
		LevelEl.push( new LevelBox( new Rect( new Pos(30,270),11,8)));
		LevelEl.push( new LevelBox( new Rect( new Pos(20,280),11,8)));
		LevelEl.push( new LevelBox( new Rect( new Pos(10,290),11,8)));
		LevelEl.push( new LevelBox( new Rect( new Pos(0,300),11,8)));
		LevelEl.push( new LevelBox( new Rect( new Pos(450,210),50,130)));
		LevelEl.push( new LevelBox( new Rect( new Pos(0,349),0,251)));
		LevelEl.push( new LevelBox( new Rect( new Pos(799,349),0,251)));
		LevelEl.push( new LevelBox( new Rect( new Pos(1,599),800,0)));
		LevelEl.push( new LevelElement( new Rect( new Pos(333,263),13,15 ), Resource.get( "img/Sprite/VinceProf.png" ), 5,0,2.5) );
		//Teleport element
		var SpawnBox = new LevelBox( new Rect( new Pos(346,590),26,3)) ;
		SpawnBox.Type = BoxType.BoxType_Teleport;
		SpawnBox.NextLevel = LevelName.Level_1;
		LevelEl.push(SpawnBox);


		//player element
		LevelEl.push(  this.Player  );


		// Dialog element
		var WelcomeDialog = new LevelBox(new Rect( new Pos(357, 451), 10,10));
		WelcomeDialog.Type = BoxType.BoxType_Dialog;
		LevelEl.push(WelcomeDialog);
			
		return new Level( BackGroundImage, LevelEl ) ;
	}
	
	if( LvlName == LevelName.Level_3 )
	{
		BackGroundImage.push( new Sprite( Resource.get( "img/Map/Wood.png" ),0,0,0,0, GlobalValue.CanvasWidth, GlobalValue.CanvasHeight,0,0,1 ) );
		LevelEl.push( new LevelBox( new Rect( new Pos(423,600),50,130)));
		LevelEl.push( new LevelBox( new Rect( new Pos(382,502),40,130)));
		LevelEl.push( new LevelBox( new Rect( new Pos(486,501),40,130)));
		LevelEl.push( new LevelBox( new Rect( new Pos(330,400),40,130)));
		LevelEl.push( new LevelBox( new Rect( new Pos(535,400),40,130)));
		LevelEl.push( new LevelBox( new Rect( new Pos(574,400),225,0)));
		LevelEl.push( new LevelBox( new Rect( new Pos(800,347),0,150)));
		LevelEl.push( new LevelBox( new Rect( new Pos(707,353),150,0)));
		LevelEl.push( new LevelBox( new Rect( new Pos(695,205),0,148)));
		LevelEl.push( new LevelBox( new Rect( new Pos(597,201),0,105)));
		LevelEl.push( new LevelBox( new Rect( new Pos(597,303),130,0)));
		LevelEl.push( new LevelBox( new Rect( new Pos(597,201),160,0)));
		LevelEl.push( new LevelBox( new Rect( new Pos(749,0),0,200)));
		LevelEl.push( new LevelBox( new Rect( new Pos(687,0),0,90)));
		LevelEl.push( new LevelBox( new Rect( new Pos(262,195),165,110)));

		LevelEl.push( new LevelBox( new Rect( new Pos(161,403),170,90)));
		LevelEl.push( new LevelBox( new Rect( new Pos(0,150),110,200)));
		//porte haut droites
		LevelEl.push( new LevelBox( new Rect( new Pos(687,0),90,0)));
		//
		LevelEl.push( new LevelBox( new Rect( new Pos(436,88),250,0)));
		LevelEl.push( new LevelBox( new Rect( new Pos(426,0),0,90)));
		LevelEl.push( new LevelBox( new Rect( new Pos(105,353),50,50)));
		LevelEl.push( new LevelBox( new Rect( new Pos(0,1),450,100)));
		LevelEl.push( new LevelBox( new Rect( new Pos(106,101),50,50)));
		LevelEl.push( new LevelBox( new Rect( new Pos(688,0),70,1)));
		LevelEl.push( new LevelBox( new Rect( new Pos(798,352),1, 50)));

		var Combatforet1 = new LevelBox( new Rect( new Pos(642,359),20,20)) ;
			Combatforet1.Type = BoxType.BoxType_Combat;
			Combatforet1.NextLevel = LevelName.Level_Fight;

			LevelEl.push( Combatforet1 );		
		LevelEl.push(  this.Player  );

		var Combatforet2 = new LevelBox( new Rect( new Pos(187,257),20,20)) ;
			Combatforet2.Type = BoxType.BoxType_Combat;
			Combatforet2.NextLevel = LevelName.Level_Fight;

			LevelEl.push( Combatforet2 );		
		LevelEl.push(  this.Player  );

		var Combatforet3 = new LevelBox( new Rect( new Pos(591,130),20,20)) ;
			Combatforet3.Type = BoxType.BoxType_Combat;
			Combatforet3.NextLevel = LevelName.Level_Fight;

			LevelEl.push( Combatforet3 );		
		LevelEl.push(  this.Player  );


		//little root to wood
			var LittleRoot = new LevelBox( new Rect( new Pos(423,582),68,20)) ;
			LittleRoot.Type = BoxType.BoxType_Teleport;
			LittleRoot.NextLevel = LevelName.Level_1;
			LevelEl.push(LittleRoot);

		return new Level( BackGroundImage, LevelEl ) ;
	}
	
	if( LvlName == LevelName.Level_Fight )
	{
		BackGroundImage.push( new Sprite( Resource.get( "img/Map/Fight.png" ),0,0,0,0, GlobalValue.CanvasWidth, GlobalValue.CanvasHeight,0,0,1 ) );
				
		return new LevelFight( BackGroundImage, LevelEl ) ;
	}

}


LevelFactory.prototype.generateCombat = function ( Pok1, Pok2 )
{
	var Level = this. generateLevel( LevelName.Level_Fight );
	Level.LevelEl.push( Pok1 );
	Level.LevelEl.push( Pok2 );
	return Level;
}

