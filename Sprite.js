// How to create a sprite
/*
	Param1: SpriteSheet, the image
	Param2-3: The place in the spriteSheet
	param4-5: Width and Height of the clip image
	param6-7:Position on the Canvas
	param8-9: Size on the canvas
	param10: scale on screen
*/
function Sprite(Spritesheet,CanvasX,CanvasY,X,Y,Width,Height,OffsetX,OffsetY,Scale )
{
	this.Spritesheet = Spritesheet;
	this.X = X;
	this.Y = Y;
	this.CanvasX = CanvasX;
	this.CanvasY = CanvasY;
	this.Width = Width;
	this.Height = Height;
	this.OffsetX = OffsetX;
	this.OffsetY = OffsetY;
	this.Scale = Scale;
}

// How to use the drawImage
/*
	Param1: SpriteSheet, the image
	Param2-3: The place in the spriteSheet
	param4-5: Width and Height of the clip image
	param6-7:Position on the Canvas
	param8-9: Size on the canvas
*/
Sprite.prototype.update = function(Updatex,Updatey)
{
	this.X = Updatex;
	this.Y = Updatey;
}

Sprite.prototype.draw = function ( Context )
{
	Context.drawImage(this.Spritesheet,this.CanvasX, this.CanvasY, this.Width, this.Height, this.X+this.OffsetX, 
		this.Y+this.OffsetY, this.Width*this.Scale, this.Height*this.Scale);
}



// How to create an Animetedsprite
/*
	Param1: SpriteSheet, the image
	Param2-3: The place in the spriteSheet
	param4-5: Width and Height of the clip image
	param6-7:Position on the Canvas
	param8-9: Size on the canvas
	param10: scale on screen
	param11: Number of frame in the animation
	param12: Time between Animation update
*/
function AnimatedSprite(Spritesheet,CanvasX,CanvasY,X,Y,Width,Height,OffsetX,OffsetY,Scale, MaxFrame,Updatetime)
{
	this.base = Sprite;
	this.base(Spritesheet,CanvasX,CanvasY,X,Y,Width,Height,OffsetX,OffsetY,Scale);
	this.Frame= 0;
	this.MaxFrame= MaxFrame;
	this.CurrentFrameTime=Date.now();
	this.LastFrameTime = Date.now();
	this.UpdateTime= Updatetime;
	this.reset =function (){this.Frame = 0;}
	this.update= function(Updatex,Updatey)
	{
		this.CurrentFrameTime= Date.now();
		if(this.CurrentFrameTime - this.LastFrameTime > this.UpdateTime)
		{
			this.Frame++;
			if (this.Frame > this.MaxFrame) this.Frame = 0;
				this.LastFrameTime=Date.now();
		}
		this.X = Updatex;
		this.Y = Updatey;
	}
}
AnimatedSprite.prototype = new Sprite;

//redefinition du draw pour un sprite anime
AnimatedSprite.prototype.draw = function( Context )
{
	Context.drawImage(	this.Spritesheet, this.CanvasX+ (this.Frame*this.Width+ +this.OffsetX*this.Frame), this.CanvasY, this.Width, this.Height, this.X, 
						this.Y+this.OffsetY, this.Width*this.Scale, this.Height*this.Scale);
}

