function Pos(X,Y)
{
	this.X = X;
	this.Y = Y;
}

function Rect( Pos, Width,Height )
{
	this.Pos = Pos;
	this.Width = Width;
	this.Height = Height;
}	

function getEl( El )
{
	return document.getElementById( El );
}

function getRand( Min, Max )
{
	return Math.floor(Math.random()*(Max-Min+1)+Min);
}


function  drawDebugRect( Ent,Context )
{
	Context.beginPath();
	Context.moveTo(Ent.Box.Pos.X,Ent.Box.Pos.Y);
	Context.lineTo(Ent.Box.Pos.X+Ent.Box.Width*Ent.Scale,Ent.Box.Pos.Y);
	Context.lineTo(Ent.Box.Pos.X+Ent.Box.Width*Ent.Scale,Ent.Box.Pos.Y+Ent.Box.Height*Ent.Scale);
	Context.lineTo(Ent.Box.Pos.X,Ent.Box.Pos.Y+Ent.Box.Height*Ent.Scale);
	Context.lineTo(Ent.Box.Pos.X,Ent.Box.Pos.Y);
	Context.stroke();
	
}

function toRad(angle) { return angle * (Math.PI / 180);}
/**
 * Determine if a rectangle contains the coordinates (x,y) within it's boundaries.
 * param {object}  rect  Object with properties: x, y, width, height.
 * param {number}  x     Coordinate position x.
 * param {number}  y     Coordinate position y.
 * return {boolean}
 */
function isCollided( Rect,X,Y )
{
	return !(X < rect.Pos.X ||
           X > rect.Pos.X + rect.Width ||
           Y < rect.Pos.Y ||
           Y > rect.Pos.Y + rect.Height);
}

/**
 * Determine if two rectangles overlap.
 * param {object}  rectA Object with properties: x, y, width, height.
 * param {object}  rectB Object with properties: x, y, width, height.
 * return {boolean}
 */

function isRectCollided( rectA,rectB )
{
  return !(rectA.Pos.X + rectA.Width < rectB.Pos.X ||
           rectB.Pos.X + rectB.Width < rectA.Pos.X ||
           rectA.Pos.Y + rectA.Height < rectB.Pos.Y ||
           rectB.Pos.Y + rectB.Height < rectA.Pos.Y);
}

function isScaledRectCollided( rectA, ScaleA ,rectB,ScaleB )
{
  return !(rectA.Pos.X + rectA.Width*ScaleA < rectB.Pos.X ||
           rectB.Pos.X + rectB.Width*ScaleB < rectA.Pos.X ||
           rectA.Pos.Y + rectA.Height*ScaleA < rectB.Pos.Y ||
           rectB.Pos.Y + rectB.Height*ScaleB < rectA.Pos.Y);
}

if (!Object.is) {
  Object.is = function(v1, v2) {
    // Algorithme SameValue
    if (v1 === v2) { //Étapes 1-5, 7-10
      //Étapes 6.b-6.b +0 !=-0
      return v1 !== 0 || 1 / v1 === 1 / v2; 
    } else {
      //Étapes 6.a: NaN == NaN
      return v1 !== v1 && v2 !== v2;
    }
  };
}

console.log('Utils.js loaded');
