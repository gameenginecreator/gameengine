var State =
{
	StateIdle : 0 ,
	StateRight : 1,
	StateLeft : 2,
	StateBottom : 3,
	StateTop : 4,
	StateIdle_Left : 5,
	StateIdle_Right : 6,
	StateIdle_Top : 7,
	StateIdle_Bottom : 8,
	State_Count : 9
};

function Entity()
{
	this.Box = new Rect( new Pos(0,0),0,0 ); 
	this.LastState = State.StateIdle;
	this.CurrentState = State.StateIdle;
}

Entity.prototype.init = function()
{
}

Entity.prototype.update = function(DeltaTime)
{
}

Entity.prototype.draw = function()
{
	
}