
// Global for Game
var LastTime=0;
var MyGame = 0;

function initGame()
{
	var Canvas = document.querySelector("canvas");
	var Ctx = Canvas.getContext("2d");
    LastTime = Date.now();
	Canvas.setAttribute('width', GlobalValue.CanvasWidth);
	Canvas.setAttribute('height', GlobalValue.CanvasHeight);
	
	MyGame = new Game();
	MyGame.loadResources();
	Resource.onReady( launchGame );
}

function launchGame()
{
	MyGame.init();
	mainUpdate(); 
}

function mainUpdate() 
{
    var Now = Date.now();
	//le delta time, temps entre 2 update
    var DeltaTime = (Now - LastTime) / 1000.0;

	//Game update and render
	MyGame.update(DeltaTime)
	MyGame.draw();
	
    LastTime = Now;
    requestAnimationFrame(mainUpdate);
}


