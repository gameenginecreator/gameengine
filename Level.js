function Level(BackGround)
{
	this.BackGround = BackGround;
	this.LevelEl = new Array();
}

function Level(BackGround,LevelEl)
{
	this.BackGround = BackGround;
	this.LevelEl = LevelEl;
	
}
Level.prototype.init = function ()
{
	for(var i=0;i<this.LevelEl.length;i++)
	{
		this.LevelEl[i].init();
	}
}

Level.prototype.update = function( DeltaTime )
{
	for(var i=0;i<this.LevelEl.length;i++)
	{
		this.LevelEl[i].update(DeltaTime);
	}
}

Level.prototype.draw = function (Context)
{
	for(var i=0;i<this.BackGround.length;i++)
	{
		this.BackGround[i].draw( Context );
	}
	for(var i=0;i<this.LevelEl.length;i++)
	{
		this.LevelEl[i].draw( Context );
	}
}