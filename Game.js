// Compatibilité browser
 var animFrame = window.requestAnimationFrame ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame    ||
            window.oRequestAnimationFrame      ||
            window.msRequestAnimationFrame     ||
            null ;

var GameState =
{
	Menu:1,
	Play:2,
	Pause:3
};

var GlobalValue = 
{
	CanvasWidth : 800,
	CanvasHeight : 600,
	DebugMode: 1,
	Message : ""
};

//Create the game var
function Game()
{
	this.Level = new Array();
	this.LastLevel = "";
	this.LastLvlName = "";
	this.GameObject = new Array();
	this.Canvas = document.querySelector("canvas");
	this.Context = this.Canvas.getContext('2d');
	this.Context.font = "20px Arial";
	this.CurrentLevel = LevelName.Level_3;
	this.PokFact = new PokemonFactory();
	
//	this.GameState = GameState.Menu;
	
}

function QuitCombat(){
	if(confirm("Voulez-vous vraiment quitter le combat?")){
		MyGame.CurrentLevel = MyGame.LastLvlName;
		MyGame.Level[MyGame.CurrentLevel] = MyGame.LastLevel;

		$('.buttons').hide();
		$(".progress").hide();
	}
	
}

Game.prototype.loadResources = function()
{
	loadResources(this);
}
//Init all the gameObject after the Resource has been loaded
Game.prototype.init = function()
{
	this.LevelFact = new LevelFactory();
	for( var i =0; i< LevelName.Level_Count; i++)
	{
		this.Level[i] = this.LevelFact.generateLevel( i );
	}
	this.initPlayerPos();

	// Test pour création des fight
	/*if(GlobalValue.DebugMode == 2){
		var Pok1 =  this.ListPok[3];
		this.ListPok[4].Side=Side.SideRight;
		var Pok2 = this.ListPok[4];

		$(".buttons").show();

		this.generateCombat( Pok1, Pok2 );
	}
*/
	this.Level[this.CurrentLevel].init();

}
Game.prototype.setLevel = function( LvlName )
{
	this.CurrentLevel = LvlName;
	this.initPlayerPos();
	this.Level[this.CurrentLevel].init();
}
Game.prototype.initPlayerPos = function()
{
	var Pl = getPlayer();
	if( this.CurrentLevel == LevelName.Level_1 )
	{
		Pl.Box.Pos.X = 321;
		Pl.Box.Pos.Y =426;
		Pl.Scale = 1.5;
	}
	if( this.CurrentLevel == LevelName.Level_2 )
	{
		Pl.Box.Pos.X = 356;
		Pl.Box.Pos.Y =449;
		Pl.Scale = 2;
	}
	if( this.CurrentLevel == LevelName.Level_3 )
	{
		Pl.Box.Pos.X = 451
		Pl.Box.Pos.Y =	540
		Pl.Scale = 1.5;
	}
}

Game.prototype.update = function( DeltaTime )
{
	this.Level[this.CurrentLevel].update( DeltaTime );
}

Game.prototype.draw = function()
{
	//draw level
	if(this.Level != 0)
		this.Level[this.CurrentLevel].draw( this.Context );
		
	if( GlobalValue.DebugMode )
	{
		this.Context.fillText(GlobalValue.Message,100,100);
	}
}

Game.prototype.CreatePok = function ( Type )
{
	return this.PokFact.generatePokemon( Type );
}

Game.prototype.generateCombat = function ( Pok1,Pok2 ) 
{
	this.LastLvlName = this.CurrentLevel;
	this.LastLevel = this.Level[this.CurrentLevel];

	this.CurrentLevel = LevelName.Level_Fight;
	this.Level[this.CurrentLevel] = this.LevelFact.generateCombat( Pok1,Pok2 );
	this.Level[this.CurrentLevel].init();


	$(".progress").show();
}

function isObjectCollided(GameObj)
{
	var Collided = false;
	
	for( var i=0; i < MyGame.Level.LevelEl.length;  i++ )
	{
		if( Object.is(MyGame.Level.LevelEl[i], GameObj) == false ) // deux objets différents
		{
			if( isScaledRectCollided(MyGame.Level[MyGame.CurrentLevel].LevelEl[i].Box, MyGame.Level[MyGame.CurrentLevel].LevelEl[i].Scale,GameObj.Box,GameObj.Scale) )
			{
				Collided = true;
				console.log( MyGame.Level.LevelEl[i]  );
			}
		}
	}
	return Collided;
}

function getObjectCollided(GameObj)
{
	var ListElementColl = new Array();
	
	for( var i=0; i < MyGame.Level[MyGame.CurrentLevel].LevelEl.length;  i++ )
	{
		if( Object.is(MyGame.Level[MyGame.CurrentLevel].LevelEl[i], GameObj) == false ) // deux objets différents
		{
			if( isScaledRectCollided(MyGame.Level[MyGame.CurrentLevel].LevelEl[i].Box, MyGame.Level[MyGame.CurrentLevel].LevelEl[i].Scale,GameObj.Box,GameObj.Scale) )
			{
				ListElementColl.push( MyGame.Level[MyGame.CurrentLevel].LevelEl[i] );
			}
		}
	}
	return ListElementColl;
}

function getPlayer()
{
	for(var i =0; i<MyGame.Level[MyGame.CurrentLevel].LevelEl.length;i++)
	{
		if( MyGame.Level[MyGame.CurrentLevel].LevelEl[i] instanceof Player )
		{
			return MyGame.Level[MyGame.CurrentLevel].LevelEl[i];
		}
	}	
}



