var Progress1 = new Progressbar("inner1", 80);
var Progress2 = new Progressbar("inner2", 60);


function Progressbar(Id, Width){
	this.Id = Id;
	this.Width = Width;

	document.getElementById(this.Id).style.width = this.Width + "%";
}

Progressbar.prototype.setWidth = function ( Width )
{
	this.Width = Width;	
	document.getElementById(this.Id).style.width = Width + "%";
}