var Nom_Player = 
{
	Hugo : 0,
	Vincent : 1,
	Charlie : 2,
	Count : 3
};

function Player(PosX,PosY)
{
	this.base = Entity;
	this.base();
	
	this.LastState = State.StateIdle_Bottom;
	this.CurrentState = State.StateIdle_Bottom;

	this.Speed = 110;
	this.Box.Pos.X = PosX;
	this.Box.Pos.Y = PosY;
	this.LastPos = new Pos(this.Box.Pos.X,this.Box.Pos.Y )
	this.Scale = 3;
	this.Box.Width = 13;
	this.Box.Height = 20;
	
	this.TimeToSwitch = 1;
	this.TimeBetSwitch = 0;
	this.ListPok = new Array();
	
	this.TabSprite = new Array( Nom_Player.Count );

	for (var i = 0; i < Nom_Player.Count; i++) {
			this.TabSprite[i] = new Array(State.State_Coun);
	}
	
	this.CurrentPlayer = Nom_Player.Hugo;
}

Player.prototype = new Entity;

Player.prototype.init = function()
{
	/* Sprite set for Hugo */
	this.TabSprite[Nom_Player.Hugo][State.StateLeft] = 	new AnimatedSprite( 	Resource.get( "img/Sprite/hugo.png"),16,61,
										this.Box.Pos.X,this.Box.Pos.Y, this.Box.Width ,this.Box.Height,15.5,0,this.Scale,2,100 );
	this.TabSprite[Nom_Player.Hugo][State.StateRight] = new AnimatedSprite( Resource.get( "img/Sprite/hugo.png"),16,85,
										this.Box.Pos.X,this.Box.Pos.Y, this.Box.Width ,this.Box.Height,15.5,0,this.Scale,2,100 );
	
	this.TabSprite[Nom_Player.Hugo][State.StateTop] = 	new AnimatedSprite( Resource.get( "img/Sprite/hugo.png"),16,35,
										this.Box.Pos.X,this.Box.Pos.Y, this.Box.Width ,this.Box.Height,15.5,0,this.Scale,2,100 );
	this.TabSprite[Nom_Player.Hugo][State.StateBottom] = new AnimatedSprite( Resource.get( "img/Sprite/hugo.png"),16,7,
										this.Box.Pos.X,this.Box.Pos.Y, this.Box.Width ,this.Box.Height,15.5,0,this.Scale,2,100 );
	
	this.TabSprite[Nom_Player.Hugo][State.StateIdle_Bottom] = new Sprite( Resource.get( "img/Sprite/hugo.png"),16,7,this.Box.Pos.X,this.Box.Pos.Y,this.Box.Width ,this.Box.Height,0,0,this.Scale );
	this.TabSprite[Nom_Player.Hugo][State.StateIdle_Right] =	new Sprite(Resource.get( "img/Sprite/hugo.png"),16,85,this.Box.Pos.X,this.Box.Pos.Y,this.Box.Width ,this.Box.Height,0,0,this.Scale );
	this.TabSprite[Nom_Player.Hugo][State.StateIdle_Top] = new Sprite(Resource.get( "img/Sprite/hugo.png"),16,35,this.Box.Pos.X,this.Box.Pos.Y,this.Box.Width ,this.Box.Height,0,0,this.Scale );
	this.TabSprite[Nom_Player.Hugo][State.StateIdle_Left] = new Sprite(Resource.get( "img/Sprite/hugo.png"),16,61,this.Box.Pos.X,this.Box.Pos.Y,this.Box.Width ,this.Box.Height,0,0,this.Scale );


	/* Sprite set for Vincent */
	this.TabSprite[Nom_Player.Vincent][State.StateLeft] = 	new AnimatedSprite( 	Resource.get( "img/Sprite/vincent.png"),16,61,
										this.Box.Pos.X,this.Box.Pos.Y, this.Box.Width ,this.Box.Height,15.5,0,this.Scale,2,100 );
	this.TabSprite[Nom_Player.Vincent][State.StateRight] = new AnimatedSprite( Resource.get( "img/Sprite/vincent.png"),16,85,
										this.Box.Pos.X,this.Box.Pos.Y, this.Box.Width ,this.Box.Height,15.5,0,this.Scale,2,100 );
	
	this.TabSprite[Nom_Player.Vincent][State.StateTop] = 	new AnimatedSprite( Resource.get( "img/Sprite/vincent.png"),16,35,
										this.Box.Pos.X,this.Box.Pos.Y, this.Box.Width ,this.Box.Height,15.5,0,this.Scale,2,100 );
	this.TabSprite[Nom_Player.Vincent][State.StateBottom] = new AnimatedSprite( Resource.get( "img/Sprite/vincent.png"),16,7,
										this.Box.Pos.X,this.Box.Pos.Y, this.Box.Width ,this.Box.Height,15.5,0,this.Scale,2,100 );
	
	this.TabSprite[Nom_Player.Vincent][State.StateIdle_Bottom] = new Sprite( Resource.get( "img/Sprite/vincent.png"),16,7,this.Box.Pos.X,this.Box.Pos.Y,this.Box.Width ,this.Box.Height,0,0,this.Scale );
	this.TabSprite[Nom_Player.Vincent][State.StateIdle_Right] =	new Sprite(Resource.get( "img/Sprite/vincent.png"),16,85,this.Box.Pos.X,this.Box.Pos.Y,this.Box.Width ,this.Box.Height,0,0,this.Scale );
	this.TabSprite[Nom_Player.Vincent][State.StateIdle_Top] = new Sprite(Resource.get( "img/Sprite/vincent.png"),16,35,this.Box.Pos.X,this.Box.Pos.Y,this.Box.Width ,this.Box.Height,0,0,this.Scale );
	this.TabSprite[Nom_Player.Vincent][State.StateIdle_Left] = new Sprite(Resource.get( "img/Sprite/vincent.png"),16,61,this.Box.Pos.X,this.Box.Pos.Y,this.Box.Width ,this.Box.Height,0,0,this.Scale );



	/* Sprite set for Charlie */
	this.TabSprite[Nom_Player.Charlie][State.StateLeft] = 	new AnimatedSprite( 	Resource.get( "img/Sprite/charlie.png"),16,61,
										this.Box.Pos.X,this.Box.Pos.Y, this.Box.Width ,this.Box.Height,15.5,0,this.Scale,2,100 );
	this.TabSprite[Nom_Player.Charlie][State.StateRight] = new AnimatedSprite( Resource.get( "img/Sprite/charlie.png"),16,85,
										this.Box.Pos.X,this.Box.Pos.Y, this.Box.Width ,this.Box.Height,15.5,0,this.Scale,2,100 );
	
	this.TabSprite[Nom_Player.Charlie][State.StateTop] = 	new AnimatedSprite( Resource.get( "img/Sprite/charlie.png"),16,35,
										this.Box.Pos.X,this.Box.Pos.Y, this.Box.Width ,this.Box.Height,15.5,0,this.Scale,2,100 );
	this.TabSprite[Nom_Player.Charlie][State.StateBottom] = new AnimatedSprite( Resource.get( "img/Sprite/charlie.png"),16,7,
										this.Box.Pos.X,this.Box.Pos.Y, this.Box.Width ,this.Box.Height,15.5,0,this.Scale,2,100 );
	
	this.TabSprite[Nom_Player.Charlie][State.StateIdle_Bottom] = new Sprite( Resource.get( "img/Sprite/charlie.png"),16,7,this.Box.Pos.X,this.Box.Pos.Y,this.Box.Width ,this.Box.Height,0,0,this.Scale );
	this.TabSprite[Nom_Player.Charlie][State.StateIdle_Right] =	new Sprite(Resource.get( "img/Sprite/charlie.png"),16,85,this.Box.Pos.X,this.Box.Pos.Y,this.Box.Width ,this.Box.Height,0,0,this.Scale );
	this.TabSprite[Nom_Player.Charlie][State.StateIdle_Top] = new Sprite(Resource.get( "img/Sprite/charlie.png"),16,35,this.Box.Pos.X,this.Box.Pos.Y,this.Box.Width ,this.Box.Height,0,0,this.Scale );
	this.TabSprite[Nom_Player.Charlie][State.StateIdle_Left] = new Sprite(Resource.get( "img/Sprite/charlie.png"),16,61,this.Box.Pos.X,this.Box.Pos.Y,this.Box.Width ,this.Box.Height,0,0,this.Scale );
	
	for( var i =1; i<=5;i++)
	{
		this.ListPok[i] = initPok(i);
	}
	
	

}
//Spritesheet,CanvasX,CanvasY,X,Y,Width,Height,OffsetX,OffsetY,Scale, Maxframe,Updatetime)

Player.prototype.update = function( DeltaTime ) 
{
	this.LastPos.X = this.Box.Pos.X
	this.LastPos.Y = this.Box.Pos.Y;

	this.TimeBetSwitch += DeltaTime;

	if (65 in KeysDown || 37 in KeysDown)  //Left
	{
		if( this.CurrentState != State.StateLeft )
			this.changeState( State.StateLeft );
		this.Box.Pos.X -= this.Speed * DeltaTime;
	}

	else if (87 in KeysDown || 38 in KeysDown) // TOP
	{
		if( this.CurrentState != State.StateTop )
			this.changeState( State.StateTop );
		this.Box.Pos.Y -= this.Speed * DeltaTime;
	}

	else if (68 in KeysDown || 39 in KeysDown)  // right
	{
		if( this.CurrentState != State.StateRight )
			this.changeState( State.StateRight );
		this.Box.Pos.X += this.Speed * DeltaTime;
	}

	else if (83 in KeysDown || 40 in KeysDown) //Down
	{
		if( this.CurrentState != State.StateBottom )
			this.changeState( State.StateBottom );
		this.Box.Pos.Y += this.Speed * DeltaTime;
	}
	else{
		this.toIdleState();
	}

	if( 67 in KeysDown )
	{
		if(this.CurrentLevel != LevelName.Level_2){
			this.switchPlayer();
		}else{
			return false;
		}
	}

	if( 27 in KeysDown){
		// Quitter le dialogue en cours
		DialogManager.erase();
	}

	if( 16 in KeysDown )
	{
		this.Speed = 210;
	}else{
		if(MyGame.CurrentLevel == "LevelName.Level_3"){
			this.Speed = 10;
		}

		this.Speed = 110;
	}
	var ListColl = getObjectCollided( this );
	
	
	// if a collision happen, just back to your last position
	if(  ListColl.length > 0 )
	{
		for( var i=0; i<ListColl.length; i++)
		{
			if( ListColl[i] instanceof  LevelBox )
			{
				if( ListColl[i].Type == BoxType.BoxType_Collision )
				{
					this.Box.Pos.X = this.LastPos.X
					this.Box.Pos.Y = this.LastPos.Y;
				}
				else if(  ListColl[i].Type == BoxType.BoxType_Teleport )
				{
					MyGame.setLevel(  ListColl[i].NextLevel );
				}
				else if(  ListColl[i].Type == BoxType.BoxType_Combat )
				{
					var NoPok1 = getRand(1,5);
					var Pok1 =  this.ListPok[NoPok1];
					var Find = false;
					var Pok2 = 0;
					while( Find == false )
					{
						var NoPok2 = getRand(1,5);
						if( NoPok2 != NoPok1 )
						{
							Pok2 = this.ListPok[NoPok2];
							Find = true;
						}
					}

					$(".buttons").show();

					this.Box.Pos.X = this.LastPos.X
					this.Box.Pos.Y = this.LastPos.Y;



					MyGame.generateCombat( Pok1, Pok2 );
				}
				else if( ListColl[i].Type == BoxType.BoxType_Dialog )
				{
						DialogManager.show("Bienvenue sur Progémon. Appuyez sur «c» pour changer de personnage. Faites bien votre choix car c'est la dernière fois ou vous pourrez décider votre personnage.");
				}
			}
		}
	 }
	this.TabSprite[this.CurrentPlayer][this.CurrentState].update( this.Box.Pos.X,this.Box.Pos.Y);
};

Player.prototype.switchPlayer = function ()
{
	if( this.TimeBetSwitch >  this.TimeToSwitch )
	{
		this.CurrentPlayer++;
		this.TimeBetSwitch = 0;
		if(this.CurrentPlayer >= Nom_Player.Count)
		{
			this.CurrentPlayer = 0; 	
		}
	}
}


Player.prototype.changeState = function( newState )
{
	this.LastSate = this.CurrentState;
	this.CurrentState = newState;
}

Player.prototype.toIdleState = function ()
{
	if( this.CurrentState == State.StateRight )
			this.changeState( State.StateIdle_Right );
	if( this.CurrentState == State.StateLeft )
			this.changeState( State.StateIdle_Left );
	if( this.CurrentState == State.StateTop )
			this.changeState( State.StateIdle_Top );
	if( this.CurrentState == State.StateBottom )
			this.changeState( State.StateIdle_Bottom );
}

Player.prototype.draw = function(Context)
{
	this.TabSprite[this.CurrentPlayer][this.CurrentState].draw(Context);
	
	if( GlobalValue.DebugMode )
	{
		drawDebugRect(this,Context);
	}
}

function initPok(NoPok)
{
	var Pok = 0;
	switch( NoPok )
	{
		case 1: var Pok =  MyGame.PokFact.generatePokemon( TypePok.Proglax, SidePokemon.SideLeft); break;
		case 2: var Pok =  MyGame.PokFact.generatePokemon( TypePok.Kary, SidePokemon.SideLeft); break;
		case 3: var Pok =  MyGame.PokFact.generatePokemon( TypePok.Vichu, SidePokemon.SideLeft); break;
		case 4: var Pok =  MyGame.PokFact.generatePokemon( TypePok.Digcky, SidePokemon.SideLeft); break;
		case 5: var Pok =  MyGame.PokFact.generatePokemon( TypePok.Wobburush, SidePokemon.SideLeft); break;
	}
	return Pok;
}
console.log('Player.js loaded');