var KeysDown = {};

var Mouse =  {
	Position: new Pos(0,0),
	Clicked: false,
	Down: false
};

window.addEventListener('keydown', function(e) 
{
	KeysDown[e.keyCode] = true;
});

window.addEventListener('keyup', function(e) 
{
	delete KeysDown[e.keyCode];
});

document.querySelector("canvas").addEventListener('mousemove', function(Event) {
	var MousePos = getMousePos(Event);
	GlobalValue.Message = 'Mouse pos: ' + Math.round( MousePos.X ) + ',' + Math.round( MousePos.Y );
}
);

document.querySelector("canvas").addEventListener('mousedown', function(Event) {
	var MousePos = getMousePos(Event);
	// do what you want here
}
);

document.querySelector("canvas").addEventListener('mouseup', function(Event) {
	var MousePos = getMousePos(Event);
	// do what you want here
}
);

	
 function getMousePos(Event) {
	var Canvas = document.querySelector("canvas");
	var Rect = Canvas.getBoundingClientRect();
	return {
		X: (Event.clientX - Rect.left) / (Rect.right - Rect.left) * Canvas.width,
        Y: (Event.clientY - Rect.top) / (Rect.bottom - Rect.top) * Canvas.height
	};
}

console.log('keyControls.js loaded');