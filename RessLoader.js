function loadResources(Game)
{
	//backGroundImage
	Resource.load(["img/Map/backZel.png"]);
	Resource.load(["img/Map/Background.png"]);
	Resource.load(["img/Map/Littleroot.png"]);
	Resource.load(["img/Map/Wood.png"]);
	Resource.load(["img/Map/Fight.png"]);
	
	//playerSprite
	
	Resource.load(["img/Sprite/hugo.png"]);
	Resource.load(["img/Sprite/vincent.png"]);
	Resource.load(["img/Sprite/charlie.png"]);
	Resource.load(["img/Sprite/VinceProf.png"]);
	
	//Pokemon
	Resource.load(["img/Pokemon/proglax.png"]);
	Resource.load(["img/Pokemon/karianne.png"]);
	Resource.load(["img/Pokemon/Vickie.png"]);
	Resource.load(["img/Pokemon/Wobburush.png"]);
	Resource.load(["img/Pokemon/Vichu.png"]);
}


(function() {
    var resourceCache = {};
    var loading = [];
    var readyCallbacks = [];

    // Load an image url or an array of image urls
    function load(urlOrArr) {
        if(urlOrArr instanceof Array) {
            urlOrArr.forEach(function(url) {
                _load(url);
            });
        }
        else {
            _load(urlOrArr);
        }
    }

    function _load(url) {
        if(resourceCache[url]) {
            return resourceCache[url];
        }
        else {
            var img = new Image();
            img.onload = function() {
                resourceCache[url] = img;

                if(isReady()) {
                    readyCallbacks.forEach(function(func) { func(); });
                }
            };
            resourceCache[url] = false;
            img.src = url;
        }
    }

    function get(url) {
        return resourceCache[url];
    }

    function isReady() {
        var ready = true;
        for(var k in resourceCache) {
            if(resourceCache.hasOwnProperty(k) &&
               !resourceCache[k]) {
                ready = false;
            }
        }
        return ready;
    }

    function onReady(func) {
        readyCallbacks.push(func);
    }

    window.Resource = { 
        load: load,
        get: get,
        onReady: onReady,
        isReady: isReady
    };
})();